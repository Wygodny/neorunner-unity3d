﻿#pragma strict

var speed = 8.0;
var jumpForce = 300.0;
var deadZone = 0.2;
private var userControlsType = null;
private var distToGround: float;
function Start () {
	distToGround = collider.bounds.extents.y;
	userControlsType = PlayerPrefs.GetString("userControlType");
	deadZone = PlayerPrefs.GetFloat("accelerometrDigitalDeadZone") || deadZone;
	speed = PlayerPrefs.GetFloat("horizontalSpeed") || speed;
}

function Update () {
	var dir : Vector3 = Vector3.zero;
	
	if (userControlsType === "ACCELEROMETR_ANALOG") {
		dir.x = Input.acceleration.x;
	} else if (userControlsType === "KEYS") {
	
	} else { //undefined albo ACCELEROMETR_DIGITAL
	    if (Input.acceleration.x < -deadZone) {
	    	dir.x = -1;
	    } else if (Input.acceleration.x > deadZone) {
	    	dir.x = 1;
	    }
	}
	
	#if UNITY_EDITOR
	if (Input.GetAxisRaw("Horizontal")) {
		dir.x = Input.GetAxisRaw("Horizontal");
	}
	#endif

    // Make it move 10 meters per second instead of 10 meters per frame...
    dir *= Time.deltaTime;

    // Move object
    transform.Translate (dir * speed);
}

function FixedUpdate() {
	//Skok
	if (IsGrounded() && ((Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)|| Input.GetKeyDown(KeyCode.Space))) 
	{
		jump();
	}
	
	//tymczasoowy napęd
	this.rigidbody.AddForce(Vector3.forward * 10, ForceMode.Acceleration );
	
}

function jump() {
	this.rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Acceleration);
}

function IsGrounded(): boolean {
	var size = this.collider.bounds.size;
	var leftBot = transform.position;
	leftBot.x -= size.x / 2;
	leftBot.z -= size.z / 2;
	
	var leftTop = leftBot;
	leftTop.z += size.z;
	
	var rightTop = leftTop;
	rightTop.x += size.x;
	
	var rightBot = rightTop;
	rightBot.z -= size.z;

	if (Physics.Raycast(leftBot, -Vector3.up, distToGround + 0.1) ||
		Physics.Raycast(leftTop, -Vector3.up, distToGround + 0.1) ||
		Physics.Raycast(rightBot, -Vector3.up, distToGround + 0.1) ||
		Physics.Raycast(rightTop, -Vector3.up, distToGround + 0.1)) 
		return true; else return false;

//	return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.01);
}








