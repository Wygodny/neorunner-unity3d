﻿#pragma strict

var lineCursor: GameObject;
var lineOfTiles: GameObject;

var linesCount = 10;
var density = 0.50;


function Start () {

}

function Update () {
	// +2 dlatego że ten obiekt zawiera dodatkowe startowe elementy
	if (transform.childCount <= linesCount + 2) {
		var position: Vector3 = new Vector3(1.0,1.0,1.0);
		var rotation: Quaternion = new Quaternion(0,0,0,0);
		var newObject: GameObject = Instantiate(lineOfTiles, lineCursor.transform.position , rotation);
		newObject.transform.parent = this.transform;
		lineCursor.transform.position.z += newObject.collider.bounds.size.z;
	}
}