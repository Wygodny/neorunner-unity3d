﻿#pragma strict


var leader: GameObject;
var offset = -20.0;

function Start () {

}

function Update () {
	var position = leader.transform.position;
	position.z += offset;
	transform.position = position;
}

function OnCollisionEnter(collision : Collision) {
	var line = collision.gameObject;
	var parent = line.transform.parent.gameObject;	
	line.Destroy(parent);	
}